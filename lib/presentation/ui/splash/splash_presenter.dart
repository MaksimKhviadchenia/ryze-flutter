import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../screens.dart';

class SplashPresenter {
  startSplashTimer(BuildContext _context) async {}
}

class BasicSplashPresenter implements SplashPresenter {
  @override
  startSplashTimer(BuildContext context) {
    var _duration = Duration(
      seconds: 3,
    );
    return Timer(_duration, () {
      nextScreen(context);
    });
  }

  void nextScreen(BuildContext context) {
    Navigator.pushReplacementNamed(context, Screens.AUTHORIZATION);
  }
}
