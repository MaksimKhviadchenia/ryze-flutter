import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rize/presentation/ui/splash/splash_presenter.dart';

class SplashScreen extends StatefulWidget {

  final SplashPresenter presenter;

  SplashScreen(this.presenter, {Key key}): super(key: key);

  @override
  _SplashScreenState createState() => new _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SvgPicture.asset('assets/images/background_dark.svg', fit: BoxFit.fill),
          Center(
           child: SvgPicture.asset('assets/images/logo.svg', fit: BoxFit.cover, width: 256, height: 256, alignment: Alignment.center,),
          )
        ],
      ),
    );
  }


  @override
  void initState() {
    super.initState();
    this.widget.presenter.startSplashTimer(context);
  }
}

