import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:rize/presentation/ui/authorization/sign_response.dart';
import 'dart:convert';

class AuthorizationPresenter {
  void signIn(String login, String password) {}

  void forwardCreateAccount(BuildContext context) {}

  void forwardForgotPassword(BuildContext context) {}
}

class BasicAuthorizationPresenter implements AuthorizationPresenter {
  @override
  void forwardCreateAccount(BuildContext context) {
    print("forwardCreateAccount");
  }

  @override
  void forwardForgotPassword(BuildContext context) {
    print("forwardForgotPassword");
  }

  @override
  void signIn(final String login, final String password) {
    http.post("https://dev-api.ryzerewards.com/api/v1/auth/sign_in/", headers: {
      "X-CLIENT": "android"
    }, body: {
      "identity": login,
      "password": password
    }).then((responce) {
      var signInResponse = SignInResponse.fromJson(json.decode(responce.body));
      print(signInResponse.token);
    }).catchError((error) {
      print(error);
    });
  }
}
