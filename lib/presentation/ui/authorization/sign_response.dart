import 'dart:convert';

class SignInResponse {
  final String token;
  final User user;

  SignInResponse({this.token, this.user});

  factory SignInResponse.fromJson(Map<String, dynamic> jsonData) {
    return SignInResponse(
      token: jsonData["auth_token"],
      user: User.fromJson(jsonData["user"])
    );
  }
}

class User {
  final String fullName;

  User({this.fullName});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      fullName: json["full_name"]
    );
  }
}