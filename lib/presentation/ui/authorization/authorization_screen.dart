import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rize/presentation/ui/authorization/authorization_presenter.dart';

class AuthorizationScreen extends StatefulWidget {

  final AuthorizationPresenter presenter;

  AuthorizationScreen(this.presenter, {Key key}) : super(key: key);

  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {

  final loginController = TextEditingController();
  final passwordController = TextEditingController();

  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
//    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return Scaffold(
        body: Stack(
          children: <Widget>[
            SvgPicture.asset('assets/images/background.svg', fit: BoxFit.fill),
            LayoutBuilder(
              builder: (context, constraints) {
                return SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: constraints.maxWidth,
                          minHeight: constraints.maxHeight),
                      child: IntrinsicHeight(
                        child: Container(
                          child: Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceAround,
                                  children: <Widget>[
                                    Center(
                                      child: SvgPicture.asset(
                                        'assets/images/logo.svg',
                                        fit: BoxFit.cover,
                                        width: 196,
                                        height: 196,
                                        alignment: Alignment.topCenter,
                                      ),
                                    ),
                                    TextField(
                                      keyboardType: TextInputType.emailAddress,
                                      controller: loginController,
                                      decoration: InputDecoration(
                                        labelText: "Email",
                                        prefixIcon: Icon(
                                          Icons.account_circle,
                                        ),
                                      ),
                                    ),
                                    TextField(
                                      controller: passwordController,
                                      decoration: InputDecoration(
                                          labelText: "Password",
                                          prefixIcon: Icon(Icons.lock),
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              _passwordVisible
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                _passwordVisible =
                                                !_passwordVisible;
                                              });
                                            },
                                          )),
                                      obscureText: _passwordVisible,
                                    ),
                                    Align(
                                        alignment: Alignment.topRight,
                                        child: InkWell(child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16.0, vertical: 16.0),
                                          child: Text("Forgot password?"),
                                        ),)
                                    ),
                                    Expanded(
                                      child: Container(
                                        width: 100,
                                        height: 100,
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: MaterialButton(
                                        child: Text("Sign In"),
                                        onPressed: () {
                                          this.widget.presenter.signIn(
                                              loginController.text,
                                              passwordController.text);
                                        },
                                        textColor: Colors.white,
                                        color: Colors.green,
                                        splashColor: Colors.redAccent,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                16.0)),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: RaisedButton(
                                        child: Text("Create accouunt"),
                                        onPressed: () {
                                          this.widget.presenter
                                              .forwardCreateAccount(context);
                                        },
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                16.0)),
                                      ),
                                    )
                                  ])),
                        ),
                      ),
                    ));
              },
            )
          ],
        ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
